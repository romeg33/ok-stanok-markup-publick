##Использование данной верстки

###PHP
* Сделать чтобы $_SERVER['DOCUMENT_ROOT'] == public_html
* composer install

###WebPack
* Установить Node.js
* Установить зависимые библиотеки для фронтэнда:  
  npm install
* запустить компиляцию фронтэнда через npm:
    npm run webpack
    или
    npm run watch
    или
    npm run wds
    для подробностей см. package.json  

Зафиксировать версии пакетов npm shrinkwrap --dev

>Исходники SASS и ES6 лежат в /assets  
Откомпилированные CSS и JS появляются в /public_html/builds

> если переменная окружения NODE_ENV !== 'dev',  
тогда WebPack минифицирует CSS и JS