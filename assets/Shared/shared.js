import './graceful-degradation.js';
import 'choices.js';
import 'nouislider';

import toggleScrolledFromTopClassToBody from '../jubilant-engine/toggleScrolledFromTopClassToBody';
toggleScrolledFromTopClassToBody({});


import MainNav from '../Includes/NavMain/MainNav.js';
MainNav();

import header from '../Includes/Header/Header.js';
header();

import NavGallery from '../Includes/NavGallerySE/NavGallerySE.js';
new NavGallery(document.querySelector('.nav-gallery-se'));

import listenScrollToHeader from '../jubilant-engine/listenScrollToHeader';
listenScrollToHeader(document.querySelector('.header-main-wrapper'));

import preloader from './preloader';
preloader();