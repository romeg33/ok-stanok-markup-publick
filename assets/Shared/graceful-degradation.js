//todo: не забыть раскомментить когда все будет готово. 250ms компиляции.
//Не удалять. Нужны все 3. Будут нужны еще несколько лет. (написано в октябре 2016)
import "babel-polyfill";                            //250kb uncompressed, 90kb compressed
import 'whatwg-fetch';                              //14kb uncompressed, 6kb compressed
import 'expose?objectFitImages!object-fit-images';  //3kb uncompressed
objectFitImages();  //for ie10/11
import ssp from 'smoothscroll-polyfill';
ssp.polyfill();

//set useragent like this
//<html data-useragent="This is browsers useragent" data-is-edge="false" data-is-ie11="false" data-is-ie10="false" data-is-microsoft="false">
//здесь-же происходит удаление <source> для edje
(function(){
    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);
    doc.setAttribute('data-is-edge', 'false');
    doc.setAttribute('data-is-ie11', 'false');
    doc.setAttribute('data-is-ie10', 'false');
    doc.setAttribute('data-is-microsoft', 'false');

    let isMicrosoft = false;


    var regexp = new RegExp("Edge", "i");
    if (regexp.test(navigator.userAgent) ){
        doc.setAttribute('data-is-edge', 'true');
        isMicrosoft = true;

        //Убрать <source>
        let sourceTags = document.querySelectorAll('source');
        Array.from(sourceTags).forEach(function(arrayTag){
            arrayTag.parentNode.removeChild(arrayTag);
        })
    }

    if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
        doc.setAttribute('data-is-ie11', 'true');
        isMicrosoft = true;
    }

    if (/MSIE 10/i.test(navigator.userAgent)) {
        doc.setAttribute('data-is-ie10', 'true');
        isMicrosoft = true;
    }
    if (isMicrosoft) {
        doc.setAttribute('data-is-microsoft', 'true');
    }
})()
