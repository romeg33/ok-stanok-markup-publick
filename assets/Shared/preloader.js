export default function preloader () {
    function hidePreloader() {
        document.querySelector('#preloader').classList.add('hidden');
    };
    window.onload = function () {
        setTimeout(hidePreloader, 500);
    };
    // document.addEventListener('DOMContentLoaded', function () {
    //     setTimeout(hidePreloader, 500);
    // });
}
