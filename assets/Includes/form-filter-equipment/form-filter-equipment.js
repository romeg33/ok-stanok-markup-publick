import noUiSlider from 'nouislider';
import commafy from '../../jubilant-engine/commafy';
import RadiosDisabler from '../../jubilant-engine/radios-disabler/radios-disabler';
// import makeRBSuncheckable from '../../jubilant-engine/makeRadiosUnceckable';
// import UnCheckableRBs from '../../jubilant-engine/UnCheckableRBs';

let formFilterEquipment = {
    _el: '',    //form element
    _chSel1: '',
    _noUiSlider1: '',
    _baseAction: '',
    _subButton: '',
    _slider: '',
    _listeners: {
        'change': [],
        'submit': [],
    },

    init: function () {
        this._el = document.querySelector('.form-filter-equipment');
        this._baseAction = this._el.action;
        this._subButton = document.querySelector('.form-filter-equipment__submit');
        this._reset = document.querySelector('.btn-clear-filter');
        this._initSelects();
        this._initSlider();
        this._initRadios();
        this._initResetButton();
        this._initSubmitButton();
        this._initChangeCbs();

        this._el.addEventListener('submit', (e)=> {
            e.preventDefault();
            e.stopPropagation();
        });
    },
    _initRadios: function () {
        let trashTypeDisabler = new RadiosDisabler({
            nlRadios: this._el.querySelectorAll('.form-filter-equipment__trash-type input[type="radio"]'),
            nForm: this._el,
            cbUncheck: (rb)=>{
                this._procChandeCbs();
            }
        });
        let wareHouseDisabler = new RadiosDisabler({
            nlRadios: this._el.querySelectorAll('.form-filter-equipment__warehouse input[type="radio"]'),
            nForm: this._el,
            cbUncheck: (rb)=>{
                this._procChandeCbs();
            }
        });
        let stateDisabler = new RadiosDisabler({
            nlRadios: this._el.querySelectorAll('.form-filter-equipment__state input[type="radio"]'),
            nForm: this._el,
            cbUncheck: (rb)=>{
                this._procChandeCbs();

            }
        });
        
    },
    _initSlider: function () {
        let elMin = this._el.querySelector('.min > p');
        let elMax = this._el.querySelector('.max > p');

        var slider = document.querySelector('.form-filter-equipment__price-gap-slider');
        this._slider = slider;

        noUiSlider.create(slider, {
            start: [slider.dataset.min, slider.dataset.max],
            connect: true,  //линия межжду точками
            range: {
                'min': parseFloat(slider.dataset.min),
                'max': parseFloat(slider.dataset.max)
            }
        });

        //возможно здесь. Переустано
        // slider.noUiSlider.on('set', (e)=> {
        slider.noUiSlider.on('change', (e)=> {
            this._procChandeCbs();
            elMin.innerHTML = '₽ ' + commafy(Math.round([e[0]]));
            elMax.innerHTML = '₽ ' + commafy(Math.round([e[1]]));


        });
    },
    _initSelects: function () {
        let sel1 = this._el.querySelector('select.form-filter-equipment--equipment-type-select');

        let chSel1 = new Choices(sel1, {
            search: false,
            sortFilter: function(a, b) {
                if (a.id < b.id || a.value == 'empty') {
                    return -1;
                } else if (a.id > b.id || b.value == 'empty') {
                    return 1;
                } else {
                    return 0;
                }
            },
            callbackOnChange: (value, elSelect) => {
                this._el.reset();
                this._slider.noUiSlider.reset();
                //this._resetAllCheckboxes(); //Это не нужно: оно и так сбрасывается при ресете формы
                this._procChandeCbs();
            }
        });
        chSel1.containerOuter.classList.add('choices--blue');
        this._chSel1 = chSel1;

    },
    _initResetButton: function () {
        this._reset.setAttribute('href', this._baseAction);
        // this._reset.addEventListener('click', ()=> {
        //     this._el.reset();
        //     this._resetAllCheckboxes();
        //     this._chSel1.setValueByChoice('empty');
        //     this._procChandeCbs();
        //     this._slider.noUiSlider.reset();
        // });
    },
    _resetAllCheckboxes(){
        let cbxs = this._el.querySelectorAll('input[type=checkbox]');
//         console.log(cbxs)
        Array.from(cbxs).forEach((cbx)=>{
            // console.log('yo')
           cbx.checked = false; 
        });
    },
    _initChangeCbs: function () {
        let inputs = this._el.querySelectorAll('input');
        Array.from(inputs).forEach((input) => {
            this._initChangeCbsFor(input);
        })
        //для плагина choices колбэки вызываются в _initSelects
        //для nouislider соответственно в _initSlider

    },
    _initChangeCbsFor: function(input) {
        input.addEventListener('change', this._procChandeCbs.bind(this));
    },
    _procChandeCbs: function () {
        // console.log('_procChandeCbs')
        this._listeners['change'].forEach((cb)=> cb());
    },
    _initSubmitButton: function () {
        // let href = this._el.querySelector('.form-filter-equipment__submit');
        // href.addEventListener('click', (e)=> {
        //     e.preventDefault();
        //     this._listeners['submit'].forEach((cb)=>cb());
        // });
    },

    getVals: function (cb = (data)=> {
    }) {
        var formData = new FormData(this._el);
        formData.append('price', this._slider.noUiSlider.get()[0] + '-' + this._slider.noUiSlider.get()[1]);
        this._updateUrl(formData);
        // for (var [key, value] of formData.entries()) {console.log(key, value);}

        fetch(this._el.action, {
            method: 'POST',
            body: formData,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(function (res) {
            return res.json();
        }).then((data)=> {
            // console.log(data);
            cb(data)
        }).catch((err)=> {
            console.log(err);
        })
    },
    _updateUrl: function(formData) {
        let urlPart = '';
        let categoryName = this._el.querySelector('select.form-filter-equipment--equipment-type-select').attributes['name'].value;
        let categoryVal  = formData.get(categoryName);

        if (categoryVal && categoryVal != 'empty') {
            urlPart = urlPart + '/' + categoryVal;
        }

        let filterPart = '';

        let types = [];
        Array.from(this._el.querySelectorAll('.form-filter-equipment__tipy .ch-box input:checked')).forEach(function(item) {
            let val = item.attributes['value'].value;
            if (val) {
                types.push(item.attributes['value'].value);
            }
        });
        types.sort()
        types = types.join('_');
        if (types) {
            filterPart = filterPart + '/tipy_' + types;
        }

        let wasteInput = this._el.querySelector('.form-filter-equipment__trash-type input:checked');
        if (wasteInput && wasteInput.attributes['value'].value) {
            filterPart = filterPart + '/otkhody_' + wasteInput.attributes['value'].value;
        }

        let materials = [];
        Array.from(this._el.querySelectorAll('.form-filter-equipment__matter-select .ch-box input:checked')).forEach(function(item) {
            let val = item.attributes['value'].value;
            if (val) {
                materials.push(item.attributes['value'].value);
            }
        });
        materials.sort()
        materials = materials.join('_');
        if (materials) {
            filterPart = filterPart + '/materialy_' + materials;
        }

        if (filterPart != '') {
            filterPart = '/f' + filterPart;
        }

        let newUrl = this._baseAction + urlPart + filterPart;
        this._subButton.href = newUrl;
        this._el.action = newUrl;
    },
    on: function (eName, cb) {
        this._listeners[eName].push(cb);
    },

}
export default formFilterEquipment;
