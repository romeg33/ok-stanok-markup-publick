import truncate from 'lodash/truncate';

import List from 'list.js';
import ListPagination from 'list.pagination.js/dist/list.pagination';

let pageSize = 10;

let tableSpares = {
    _options: {
        valueNames: [{name: 'link', attr: 'href'}, 'articul', {name: 'img', attr: 'src'}, 'name', 'description', 'price'],
        item: `<div class="item">
                    <a href="" class="link">
                        <div class="box-img">   
                               <img class="img" src="" />
                        </div>
                        <div class="box-name">
                              <span class="name"></span>
                        </div>
                        <div class="box-description">
                            <span class="description user-text"></span>
                        </div>
                        <div class="box-articul">
                               <span class="articul"></span>
                        </div>
                        <div class="box-price">
                             <span class="price"></span>
                        </div>     
                   </a>
           </div>`,
        indexAsync: true,   //def false
        page: pageSize,            //def 200
        plugins: [ListPagination({outerWindow: 1, innerWindow: (window.innerWidth < 800)? 2 : 3})],        //def undefined
    },
    _listJs: '',
    _chSort: '',

    init: function () {
        this._listJs = new List('table-spares', this._options);
        this._initSortSelect();
    },
    _initSortSelect (){

        this._chSort = new Choices('.table-spares__sort-select', {
          search: false,
          sortFilter: () => false,
          callbackOnChange: (value, elSelect) => {
            // this._listeners['sortselectchange'].forEach((cb)=> cb(value, elSelect));
            console.log(value)
            switch(value) {
                case 'price-asc':
                    this._listJs.sort('price', { order: "asc" });
                    break
                case 'price-desc':
                    this._listJs.sort('price', { order: "desc" });
                    break
                case 'name-asc':
                    this._listJs.sort('name', { order: "asc" });
                    break
                case 'name-desc':
                    this._listJs.sort('name', { order: "desc" });
                    break
                case 'articul-asc':
                    this._listJs.sort('articul', { order: "asc" });
                    break
                case 'articul-desc':
                    this._listJs.sort('articul', { order: "desc" });
                    break
                // case 'description-asc':
                //     this._listJs.sort('description', { order: "asc" });
                //     break
                // case 'description-desc':
                //     this._listJs.sort('description', { order: "desc" });
                //     break
                // case 'empty':
                    //
                    break
                default:
                  //
                  break
              }
          }
        });
    },
    updateValues: function (vals = []) {
        //обрезать длину текста
        // vals.forEach((val)=>{
        //     val['name'] = truncate(val['name'],{
        //        'length': 50,
        //        'separator': ' '
        //     });
        //     val['description'] = truncate(val['description'],{
        //         'length': 150,
        //         'separator': ' '
        //     });
        // });
        this._listJs.clear()
        this._listJs.add(vals);
        this._listJs.update();
        this._listJs.show(1, pageSize);
    },
}
export default tableSpares;
