export default function MainNav() {
    //hamburger

    let mainNav = document.querySelector('.main-nav');
    let menu = mainNav.querySelector('ul.menu');

    function toggleNav() {
        if (mainNav.classList.contains('main-nav_closed')) {
            mainNav.classList.remove('no-transition');
            mainNav.classList.remove('main-nav_closed');
            setTimeout(function () {
                mainNav.classList.add('no-transition');
            }, 1000);
            this.classList.add('hamburger_open');
        } else {
            mainNav.classList.remove('no-transition');
            mainNav.classList.add('main-nav_closed');
            setTimeout(function () {
                mainNav.classList.add('no-transition');
            }, 1000);
            this.classList.remove('hamburger_open');
        }
    }

    var hamburgers = document.querySelectorAll('.hamburger');
    Array.from(hamburgers).forEach(function (hamburger) {
        hamburger.addEventListener('click', toggleNav.bind(hamburger));
    });

    //hamburger end

    //submenus
    let menuItemsL1 = menu.querySelectorAll('a.js-level1');
    
    function showSubmenu(e){
        e.preventDefault();
        // console.dir('show');
        let subMenuL2 = this;
        subMenuL2.classList.add('visible');
    }
    function hideSubmenu(e){
        // console.dir('hide');
        e.preventDefault();
        let subMenuL2 = this;
        subMenuL2.classList.remove('visible');
    }
    function toggleSubmenu(e){
        e.stopPropagation();
        e.preventDefault();
        let menuItem = this;
        
        let subMenuL2 = menuItem.nextElementSibling;
        if (subMenuL2.classList.contains('visible')) {
            hideSubmenu.bind(subMenuL2, e)();
        } else {
            //спрятать все ослатльные 
            Array.from(menuItemsL1).forEach(function(menuItemL1){
                let subMenuL2 = menuItemL1.nextElementSibling;
                if (subMenuL2) {
                    hideSubmenu.bind(subMenuL2, e)();
                }
            });
            
            showSubmenu.bind(subMenuL2, e)();
        }
    }
    
    Array.from(menuItemsL1).forEach(function(menuItemL1){
        let subMenuL2 = menuItemL1.nextElementSibling;
        if (subMenuL2) {
            if ('ontouchstart' in window) {
                menuItemL1.addEventListener("click", toggleSubmenu, true);
            } else {
                menuItemL1.parentNode.addEventListener("mouseenter", showSubmenu.bind(subMenuL2));
                menuItemL1.parentNode.addEventListener("mouseleave", hideSubmenu.bind(subMenuL2));
            }
        }
    });
    //submenus end
}