// phones__link-for-modal

import Alertino from '../../jubilant-engine/components/je-alertino/je-alertino';
import ToughGuy from '../../jubilant-engine/tough-guy/tough-guy';
import autosize from 'autosize';

let jeAlertino = new Alertino();
function initModal() {
    let link = document.querySelector('.phones__link-for-modal');
    link.addEventListener('click', (e)=> {
        e.preventDefault();
        e.stopPropagation();
        // console.log('hell');
        if ('ontouchstart' in window) {
            //для малых
            let nodeToInsert = document.querySelector('.order-callback-modal-content-touch').cloneNode(true);
            jeAlertino.setContentNode(nodeToInsert);
            jeAlertino.setCssClassModifier('je-alertino--order-callback')
            jeAlertino.setCssClassBefore('order-callback-triangle-top');
            jeAlertino.setCssClassAfter('order-callback-triangle-bottom');
            jeAlertino.open();
        }
        else {
            //для больших
            let nodeToInsert = document.querySelector('.order-callback-modal-content-no-touch').cloneNode(true);
            autosize(nodeToInsert.querySelector('textarea'));
            jeAlertino.setContentNode(nodeToInsert);
            jeAlertino.setCssClassModifier('je-alertino--order-callback')
            jeAlertino.setCssClassBefore('order-callback-triangle-top');
            jeAlertino.setCssClassAfter('order-callback-triangle-bottom');
            jeAlertino.open();
            let form = nodeToInsert.querySelector('form');
            var toughGuy = new ToughGuy({
                form: form,
                onValid: () => {
                    jeAlertino.setContentHTML('<br><p>Ваша заявка отправлена.</p><br>');

                    var formData = new FormData(form);
                    //Сюда поставить правильный url обработчика
                    // fetch('/url', {
                    fetch(form.getAttribute('action'), {
                        method: 'POST',
                        body: formData,
                        credentials: 'same-origin', //send cookie for current domain
                        credentials: 'include',     //send cookie for CORS
                    }).then(function (response) {
                    }).then(function (data) {
                    })
                    setTimeout(()=> {
                        jeAlertino.close();
                    }, 2000)
                },
                hints: {
                    name: {
                        patternMismatch: 'Минимальная длина: 3 символа.',
                        valueMissing: 'Укажите Ваше имя.'
                    },
                    email: {
                        valueMissing: 'Укажите Ваш почтовый адрес.',
                        patternMismatch: 'Укажите верный почтовый адрес.',
                    },

                    phone: {
                        valueMissing: 'Укажите Ваш телефон.',
                        patternMismatch: 'Укажите верный телефон.',
                    },
                    question: {
                        errorSelector: 'например по умолчанию',
                        valueMissing: 'Поле должно быть заполнено ё',
                        typeMismatch: 'Значение должно быть например mail или url ё',
                        patternMismatch: 'значение не подходит под паттерн ё',
                        tooLong: 'превышена максимальная длина, ё',
                        rangeOverflow: 'значение превышает максимально допустимое ё',
                        rangeUnderflow: 'значение меньше чем минимально допустимое ё',
                        stepMismatch: 'ошибка шага',
                    }
                }
            });
        }

    })

}

function header() {
    initModal();
    //сгенерить искусственное открытие
    // setTimeout(()=> {
    //     let link = document.querySelector('.phones__link-for-modal');
    //     var myE = document.createEvent("Event");
    //     myE.initEvent("click", true, true);
    //     link.dispatchEvent(myE);
    // }, 300)
}

export default header;
