import isElementHover from '../../jubilant-engine/isElementHover';

class Animation {
    
    constructor(el){
        this._el = el;
        this._onAnimationEndCB;
        this._blockChildsTransitions();
        this._doStep2 = this._doStep2.bind(this);
        this._doStep3 = this._doStep3.bind(this);
        this._finishAnimation = this._finishAnimation.bind(this);
        Animation._state = 'closed';    //closed/open/go-up/go-down
    }

    _blockChildsTransitions(){
        Array.from(this._el.querySelectorAll('*'))
            .forEach(child => child.addEventListener('transitionend', e => e.stopPropagation()))
    }

    animate(onAnimationEnd){
            // this._onAnimationEndCB = onAnimationEnd;
            this._doStep1();
    }
    _finishAnimation(e){
        e.target.removeEventListener('transitionend', this._finishAnimation);
        Animation._isInProgress = false;
        this._onAnimationEndCB();
    }
    _removeAllClasses(){
        this._el.classList.remove('nav-gallery-se_state1');
        this._el.classList.remove('nav-gallery-se_state2');
        this._el.classList.remove('nav-gallery-se_state3');
        this._el.classList.remove('nav-gallery-se_state4');
    }
}

class AnimationUp extends Animation {
    constructor(el){
        super(el);
    }
    
    animate(onAnimationEnd){
        this._onAnimationEndCB = onAnimationEnd;
        // console.dir('yo')
        if (Animation._state == 'closed') {
            Animation._state = 'go-up'
            super.animate(onAnimationEnd)
        } else {
            this._onAnimationEndCB();
        }
    }
    
    _doStep1(){
        this._removeAllClasses();
        this._el.classList.add('nav-gallery-se_state2');
        this._el.addEventListener('transitionend', this._doStep2);
    }
    _doStep2(e){
        if (e.propertyName != 'opacity') {return}
        e.target.removeEventListener('transitionend', this._doStep2);
        
        this._removeAllClasses();
        this._el.classList.add('nav-gallery-se_state3');
        this._el.addEventListener('transitionend', this._doStep3);
    }
    _doStep3(e){
        if (e.propertyName != 'opacity') {return}
        e.target.removeEventListener('transitionend', this._doStep3);

        this._removeAllClasses();
        this._el.classList.add('nav-gallery-se_state4');
        this._el.addEventListener('transitionend', this._finishAnimation);
    }
    _finishAnimation(e){
        if (e.propertyName != 'opacity') {return}
        Animation._state = 'open';
        super._finishAnimation(e);
    }
}

class AnimationDown extends Animation {
    constructor(el){
        super(el);
    }
    animate(onAnimationEnd){
        this._onAnimationEndCB = onAnimationEnd;
        if (Animation._state == 'open') {
            Animation._state = 'go-down'
            super.animate(onAnimationEnd)
        } else {
            this._onAnimationEndCB();
        }
    }
    _doStep1(){
        this._removeAllClasses();
        this._el.classList.add('nav-gallery-se_state3');
        this._el.addEventListener('transitionend', this._doStep2);
    }
    _doStep2(e){
        if (e.propertyName != 'opacity') {return}
        e.target.removeEventListener('transitionend', this._doStep2);

        this._removeAllClasses();
        this._el.classList.add('nav-gallery-se_state2');
        this._el.addEventListener('transitionend', this._doStep3);
    }
    _doStep3(e){
        if (e.propertyName != 'opacity') {return}
        e.target.removeEventListener('transitionend', this._doStep3);

        this._removeAllClasses();
        this._el.classList.add('nav-gallery-se_state1');
        this._el.addEventListener('transitionend', this._finishAnimation);
    }
    _finishAnimation(e){
        if (e.propertyName != 'opacity') {return}
        Animation._state = 'closed';
        super._finishAnimation(e);
    }
}



class NavGallery {
    constructor(el){
        this._el = el;
        this._animationUp = new AnimationUp(this._el);
        this._animationDown = new AnimationDown(this._el);
        this._activate = this._activate.bind(this);
        this._deactivate = this._deactivate.bind(this);

        
        
        this._el.addEventListener('mouseenter', this._activate);
        this._el.addEventListener('mouseleave', this._deactivate);
        this._initItemHover();
        
    }
    _activate(){
        this._el.removeEventListener('mouseleave', this._deactivate);
        this._animationUp.animate(() => {
            if (!isElementHover(this._el)) this._deactivate();
                this._el.addEventListener('mouseleave', this._deactivate);
                this._processItemHover();
            }
        );
    }
    _deactivate(){
        this._el.removeEventListener('mouseenter', this._activate);
        this._animationDown.animate(() => {
                if (isElementHover(this._el)) this._activate();
                this._el.addEventListener('mouseenter', this._activate);
            }
        );
    }
    _processItemHover() {
        let items = this._el.querySelectorAll('.nav-gallery-se__item');
        Array.from(items).forEach(function (item) {
            if (isElementHover(item)) {
                item.classList.add('hover');
            } else {
                item.classList.remove('hover');
            }
        });
    }
    _initItemHover(){
        let items = this._el.querySelectorAll('.nav-gallery-se__item');
        Array.from(items).forEach((item) => {
            item.addEventListener('mouseenter', () => {
                if (this._el.classList.contains('nav-gallery-se_state4')) {
                    item.classList.add('hover');
                }
            });
            item.addEventListener('mouseleave', () => {
                item.classList.remove('hover');
            });
        });
    }
}

export default  NavGallery;