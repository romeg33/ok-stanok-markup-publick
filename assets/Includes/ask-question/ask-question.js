import Alertino from '../../jubilant-engine/components/je-alertino/je-alertino';
import setHoverClassToChilds from '../../jubilant-engine/setHoverClassToChilds';
import autosize from 'autosize';
import ToughGuy from '../../jubilant-engine/tough-guy/tough-guy';

let elAskQuestion = document.querySelector('.ask-question');
setHoverClassToChilds({
    selectorParent: '.ask-question',
    selectorChilds: '.ask-question__btn'
});

let jeAlertino = new Alertino();

function temp() {

    let askQuestionFormBlock = document.querySelector('.ask-question-form').cloneNode(true);
    autosize(askQuestionFormBlock.querySelector('textarea'));

    // jeAlertino.debug();
    jeAlertino.setCssClassModifier('je-alertino--ask-question');
    jeAlertino.setCssClassAfter('ask-question-triangle-bottom');
    jeAlertino.setCssClassBefore('ask-question-triangle-top');
    jeAlertino.setContentNode(askQuestionFormBlock);
    jeAlertino.open();

    let form = askQuestionFormBlock.querySelector('form');
    var toughGuy = new ToughGuy({
        form: form,
        onValid: () => {
            jeAlertino.setContentHTML('<br><p>Ваша заявка отправлена.</p><br>');

            var formData = new FormData(form);
            //Сюда поставить правильный url обработчика
            fetch(form.action, {
                method: 'POST',
                body: formData,
                credentials: 'same-origin', //send cookie for current domain
                credentials: 'include',     //send cookie for CORS
            }).then(function(response){}).then(function(data){})
            setTimeout(()=>{
                jeAlertino.close();
            }, 2000)
        },
        hints: {
            name: {
                patternMismatch: 'Минимальная длина: 3 символа.',
                valueMissing: 'Укажите Ваше имя.'
            },
            email: {
                valueMissing: 'Укажите Ваш почтовый адрес.',
                patternMismatch: 'Укажите верный почтовый адрес.',
            },
            question: {
                errorSelector: 'например по умолчанию',
                valueMissing: 'Поле должно быть заполнено ё',
                typeMismatch: 'Значение должно быть например mail или url ё',
                patternMismatch: 'значение не подходит под паттерн ё',
                tooLong: 'превышена максимальная длина, ё',
                rangeOverflow: 'значение превышает максимально допустимое ё',
                rangeUnderflow: 'значение меньше чем минимально допустимое ё',
                stepMismatch: 'ошибка шага',
            }
        }
    });
    // setTimeout(()=> {
    //     var e = document.createEvent("Event");
    //     e.initEvent("submit", true, true);
    //     form.dispatchEvent(e);
    // }, 300)

}
function processModal() {

    // setTimeout(temp, 830)
    elAskQuestion.addEventListener('click', e => {
        e.preventDefault();
        e.stopPropagation();

        temp();
    });

}

export default function askQuestion() {
    processModal();
};
