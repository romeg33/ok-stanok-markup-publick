import onSingleTransitionEnd from '../onSingleTransitionEnd';
import createNodeFromHTMLString from '../createNodeFromHTMLString';
import Hint from './hint';

class Enemy {
    static _classInvalid() {
        return 'tg-invalid';
    }

    static _classValid() {
        return 'tg-valid';
    }

    constructor({el, toughGuy, hintTexts}) {
        //props api
        this._el = el;
        this._toughGuy = toughGuy;
        this._hintTexts = hintTexts;

        //props internal
        this._state = 'unvalidated'         //unvalidated/valid/invalid
        this._errTypePrev = false;          //false or standard types

        this._initEl();
        this._hint = new Hint({enemy: this});
    }

    get el() {
        return this._el;
    }

    get hintTexts(){
        return this._hintTexts;
    }

    get errTypePrev(){
        return this._errTypePrev;
    }
    _isCheckDisabled(){
        return !this._toughGuy.canCheckBeDone();
    }
    _initEl() {
        this._el.addEventListener('input', e => {
            if (this._isCheckDisabled()) {return}

            this.checkValidity() ? this.win() : this.lose();


        });
    }

    /**
     * @returns {boolean/string} bool или стандартный тип ошибки браузерной валидации
     */
    getErrTypeCurr() {
        let result = false;
        switch (true) {
            case this._el.validity.valueMissing:
                result = 'valueMissing';
                break;
            case this._el.validity.typeMismatch:
                result = 'typeMismatch';
                break;
            case this._el.validity.patternMismatch:
                result = 'patternMismatch';
                break;
            case this._el.validity.tooLong:
                result = 'tooLong';
                break;
            case this._el.validity.rangeOverflow:
                result = 'rangeOverflow';
                break;
            case this._el.validity.rangeUnderflow:
                result = 'rangeUnderflow';
                break;
            case this._el.validity.stepMismatch:
                result = 'stepMismatch';
                break;
        }
        return result
    }

    _setErrorMessage() {
        let errorText = this._hintTexts[this.getErrTypeCurr()] || '';
        this._el.setCustomValidity(errorText);
    };

    lose() {
        if (this._state != 'invalid') {
            this._el.classList.remove(Enemy._classValid());
            this._el.classList.add(Enemy._classInvalid());
            this._state = 'invalid';
        }
        this._hint.lose();
        this._errTypePrev = this.getErrTypeCurr();
    }

    win() {
        if (this._state != 'valid') {
            this._el.classList.remove(Enemy._classInvalid());
            this._el.classList.add(Enemy._classValid());
            this._state = 'valid';
        }
        this._hint.win();
        this._errTypePrev = this.getErrTypeCurr();
    }

    /**
     *
     * @returns {*|boolean}
     */
    checkValidity() {
        this._setErrorMessage();
        let result = this._el.checkValidity();
        return result;
    }
}

export default Enemy;
