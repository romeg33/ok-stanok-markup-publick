import onSingleTransitionEnd from '../onSingleTransitionEnd';
import createNodeFromHTMLString from '../createNodeFromHTMLString'

let template = `
        <span class="tg-error" style="outline: 1px solid transparent">
            <span class="tg-error__content"></span>
            
        </span>
        `;

class Hint {
    constructor({enemy = false}){
        this._enemy = enemy;
        this._isAnmNow = false;
        this._el = createNodeFromHTMLString(template);
        this._content = this._el.querySelector('.tg-error__content');
        this._state = 'closed'         //closed/open
        //убрать проверку на this._el.hintTexts?
        if ( this._el.hintTexts
            && 'errorSelector' in this._el.hintTexts
            && document.querySelector(this._el.hintTexts['errorSelector'])
        ) {
            //Вставить в указанный селектор. Не опробованно.
            let elErrorWrapper = document.querySelector(this._el.hintTexts['errorSelector']);
            elErrorWrapper.appendChild(this._el);
        } else {
            //вставить сразу после элемента формы
            // elRef.parentNode.insertBefore(elNew, elRef.nextSibling);    //insertAfter не проверено
            this._enemy._el.parentNode.insertBefore(this._el, this._enemy.el.nextSibling);    //insertAfter не проверено
        }
    }
    get el(){
        return this._el;
    }

    _isNowRightAndBeforeRight(errTypePrev){
        return !this._enemy.getErrTypeCurr() &&
                !errTypePrev;
    }
    _isNowRightAndBeforeWrong(errTypePrev){
        return !this._enemy.getErrTypeCurr()
                && errTypePrev;
    }
    _isNowWrongAndBeforeRight(errTypePrev){
        return this._enemy.getErrTypeCurr() &&
                !errTypePrev;
    }
    _isNowWrongAndBeforeWrongAndErrorSame(errTypePrev) {
        return this._enemy.getErrTypeCurr() &&
                errTypePrev &&
                errTypePrev == this._enemy.getErrTypeCurr();
    }
    _isNowWrongAndBeforeWrongAndErrorOther(errTypePrev) {
        return this._enemy.getErrTypeCurr() &&
                errTypePrev &&
                errTypePrev != this._enemy.getErrTypeCurr();
    }

    lose() {
        if (this._isAnmNow) {return}
        this._isAnmNow = true;

        let errPrevStart = this._enemy.errTypePrev;
        let errCurrStart = this._enemy.getErrTypeCurr();

        let anmF;
        if (this._isNowWrongAndBeforeRight(errPrevStart)){
            anmF = this._anmAppear;
        }
        if (this._isNowWrongAndBeforeWrongAndErrorOther(errPrevStart)){
            anmF = this._anmUpdate;
        }
        if (this._isNowWrongAndBeforeWrongAndErrorSame(errPrevStart)){
            anmF = this._anmDoNothing;
        }

        anmF = anmF.bind(this);


        anmF(()=> {
            this._fixPostAnmHussle(errPrevStart, errCurrStart);
        });
    }

    win (){
        if (this._isAnmNow) {return}
        this._isAnmNow = true;

        let errPrevStart = this._enemy.errTypePrev;
        let errCurrStart = this._enemy.getErrTypeCurr();

        this._anmDisappear(()=> {
            this._fixPostAnmHussle(errPrevStart, errCurrStart);
        });
    }

    _fixPostAnmHussle(errPrevStart, errCurrStart){
        //я так и не понял почему для одних проверок первый параметр, а для других проверок - второй.
        // console.log('--------------------')
        let anmF;
        anmF =  this._anmDoNothing
        if (this._isNowRightAndBeforeRight(errPrevStart)) {
            // console.log('_isNowRightAndBeforeRight');
            //работает
            if (this._state == 'open') anmF = this._anmDisappear;
        }
        if (this._isNowRightAndBeforeWrong(errPrevStart)) {
            // console.log('_isNowRightAndBeforeWrong')
            //работает
            if (this._state == 'open') anmF = this._anmDisappear;
        }
        if (this._isNowWrongAndBeforeRight(errPrevStart)){
            // console.log('_isNowWrongAndBeforeRight')
            //работает
            if (this._state == 'closed') anmF = this._anmAppear;
        }

        if (this._isNowWrongAndBeforeWrongAndErrorSame(errPrevStart)) {
            // console.log('_isNowWrongAndBeforeWrongAndErrorSame');
            //работает
            if (this._state == 'closed') anmF = this._anmAppear;
            //если открыт, но ошибка не та - обновить   //работает
            if (this._state == 'open' && ( errCurrStart != this._enemy.getErrTypeCurr()))  anmF = this._anmUpdate;

        }
        //
        if (this._isNowWrongAndBeforeWrongAndErrorOther(errPrevStart)) {
            // console.log('_isNowWrongAndBeforeWrongAndErrorOther')
            //если закрыт - открыть //не встретилось
            if (this._state == 'closed') anmF = this._anmAppear;
            //если открыт, но ошибка до сих пор та-же самая - обновить //не встретилось
            if (this._state == 'open' && ( errPrevStart == this._enemy.getErrTypeCurr()))  anmF = this._anmUpdate;
        }

        // console.log(this._state);
        // console.log(errPrevStart);
        // console.log(errCurrStart);
        // console.log(this._enemy.getErrTypeCurr());
        // console.log(anmF.name);

        anmF = anmF.bind(this);
        anmF(()=> {
            this._isAnmNow = false;
        });

    }

    _anmAppear(cb){
        this._writeText();
        this._el.classList.add('tg-error--open');
        onSingleTransitionEnd(this._el,'max-height', ()=>{
            this._el.classList.add('tg-error--visible');
            onSingleTransitionEnd(this._content,'opacity', ()=>{
                this._state = 'open';
                cb();

            })
        })

    }
    _anmDisappear(cb){
        this._el.classList.remove('tg-error--visible');
        onSingleTransitionEnd(this._content,'opacity', ()=>{
            this._el.classList.remove('tg-error--open');
            onSingleTransitionEnd(this._el,'max-height', ()=>{
                this._state = 'closed';
                cb();
            });
        });
    }
    _anmUpdate(cb) {
        //сделать прозрачным
        this._el.classList.remove('tg-error--visible');
        onSingleTransitionEnd(this._content, 'opacity', ()=>{
            //сделать прозрачным end
            //изменить высоту
            let oldH = this._el.offsetHeight;
            this._el.style.height = oldH + 'px';
            this._writeText();
            let newH = this._content.offsetHeight;
            this._el.style.height = newH + 'px';
            onSingleTransitionEnd(this._el, 'height', ()=>{
                this._el.style.height = '';
                //изменить высоту end
                //сделать непрозрачным
                this._el.classList.add('tg-error--visible');
                onSingleTransitionEnd(this._content, 'opacity', ()=>{
                    //сделать непрозрачным end
                    cb();
                });
            });
        });
    }
    _anmDoNothing(cb){
        cb();
    }

    _writeText(){
        this._el.querySelector('.tg-error__content').innerHTML = this._enemy.el.validationMessage;
    }
}

export default Hint;
