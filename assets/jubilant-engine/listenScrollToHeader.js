//Usage in shared.js
//import listenScrollToHeader from '../jubilant-engine/listenScrollToHeader';
//listenScrollToHeader(document.querySelector('.header-wrapper'));
//
//Usage in deep childs:
//el.dispatchEvent(new Event('scroll-to-header', {'bubbles': true}));

function listenScrollToHeader(elHeader){
    let body = document.querySelector('body');
    body.addEventListener('scroll-to-header', (e)=>{
        let elToScrollStart = e.target.getBoundingClientRect().top + window.pageYOffset;
        let topOffset = elToScrollStart - elHeader.clientHeight;
        window.scroll({ top: topOffset, left: 0, behavior: 'smooth' });
    });
}

export default listenScrollToHeader;
