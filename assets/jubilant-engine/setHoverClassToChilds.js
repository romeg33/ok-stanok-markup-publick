/*
 <a class="jeHoverChilds"></a>
import setHoverClassToChilds from 'jubilant-engine/setHoverClassToChilds';
setHoverClassToChilds({
    selectorParent: '#js-unique-id',
    selectorChilds: '.jeWithImage, p.more'
});
*/

let setHoverClassToChilds = function({selectorParent = '.jeHoverChilds', selectorChilds = '*'}){
    let parents = document.querySelectorAll(selectorParent);
    Array.from(parents).forEach(function(parent){
        let childs = parent.querySelectorAll(selectorChilds);
        Array.from(childs).forEach(function(child){
            parent.addEventListener('mouseenter',function(){
                child.classList.add('hover');
            });
            parent.addEventListener('mouseleave',function(){
                child.classList.remove('hover');
            });
        });
    });
};

export default setHoverClassToChilds;