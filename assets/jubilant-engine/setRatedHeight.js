function setRatedHeight(node, ratio=1){
    node.offsetHeight = node.offsetWidth * ratio;
}
export default setRatedHeight;