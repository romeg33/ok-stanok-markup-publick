//Used in:
/*
    validator
    modal
*/
import camelCase from 'lodash/camelCase';
/**
 * Transition должен быть более 20мс, т.к. за это время определяется пошло ли transition или нет
 * @param el
 * @param prop. воодить как имя-свойства. Проверено.
 * @param cb;
 */
let onSingleTransitionEnd = function(el, prop, cb) {
    let propCC = camelCase(prop);
    let val1 = getComputedStyle(el)[propCC];
    setTimeout(()=>{
        let val2 = getComputedStyle(el)[propCC];
        if (val1 == val2) {
            //transition не происходит, т.к. за первые 20мс значение не поменялось
            cb();
        }
        else {
            el.addEventListener('transitionend', function temp(e) {
                if (e.propertyName != prop) { return }
                if (!el.isSameNode(e.target)) { return }
                el.removeEventListener('transitionend', temp);
                cb()
            });
        }
    }, 20)
}

export default onSingleTransitionEnd;
