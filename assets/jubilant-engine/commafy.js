/*
* Отсюда http://stackoverflow.com/questions/6784894/add-commas-or-spaces-to-group-every-three-digits
* Добавляет разделители к цифрам
*
* Доработал. Предстоит еще доработать
* */

function commafy( num ) {
    var futureSpacer = ' ';
    var currentSpacer = ',';
    var str = num.toString().split('.');
    if (str[0].length >= 5) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1' + futureSpacer);
    }
    if (str[1] && str[1].length >= 5) {
        str[1] = str[1].replace(/(\d{3})/g, '$1' + futureSpacer);
    }
    return str.join(currentSpacer);
}

export default commafy;
