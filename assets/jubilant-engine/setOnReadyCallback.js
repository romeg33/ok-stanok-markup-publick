let setOnReadyCallback = function(fn) {
    // uninitialized - Has not started loading yet
    // loading - Is loading
    // loaded - Has been loaded
    // interactive - Has loaded enough and the user can interact with it
    // complete - Fully loaded
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

export default setOnReadyCallback;