//используется в:
/*
    je-alertino
*/

//подразумевается что в строке корневой элемент всего 1.
//попробовать сократить на новом проекте, и назвать parseHTML
export default function createNodeFromHTMLString(str) {
    var tempElWrapper = document.createElement('div');
    var div = document.createElement('div');
    div.innerHTML = str;
    while (div.children.length > 0) {
        tempElWrapper.appendChild(div.children[0]);
    }
    return tempElWrapper.firstChild;
}
