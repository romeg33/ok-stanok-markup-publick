/*Usage
import setEqualWidthToElements from 'jubilant-engine/setEqualWidthToElements';
setTimeout(function(){
    setEqualWidthToElements({selector: '.services-list p.name'});
},1);
*/

import _ from 'lodash';
let setEqualWidthToElements = function ({selector='.someSelector'}) {
    let elementsToBeEqual = document.querySelectorAll(selector);

    function collate() {
        let maxWidth = 0;
        Array.from(elementsToBeEqual).forEach(function (element) {
            let elementWidth = element.offsetWidth;
            maxWidth = maxWidth < elementWidth ? elementWidth : maxWidth;
        });
        Array.from(elementsToBeEqual).forEach(function (element) {
            element.style.width = maxWidth + 1 + 'px';
        });
    }

    collate();
    window.addEventListener('resize', _.debounce(collate, 150));
}


export default setEqualWidthToElements;
