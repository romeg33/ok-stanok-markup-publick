import _ from 'lodash';

//вроде можно сделать флекс-боксом. Уточнить и написать результат здесь.
export default function makeFooterSticky(footerSelector = '.footer') {
    let html = document.querySelector('html');
    let body = document.querySelector('body');
    let footer = document.querySelector(footerSelector);

    html.style.position = 'relative';
    html.style.minHeight = '100%';

    footer.style.position = 'absolute';
    footer.style.bottom = '0';

    body.style.margin = '0';

    function setBodyBottomMargin(){
        let footerHeight = footer.offsetHeight;
        body.style.marginBottom = footerHeight + 'px';
    }

    setBodyBottomMargin();

    window.addEventListener('resize', _.debounce(function(){
        setBodyBottomMargin();
    }, 150));
}
