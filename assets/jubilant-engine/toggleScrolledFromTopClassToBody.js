/*
adds .scrolled-from-top to body

usage
import toggleScrolledFromTopClassToBody from 'jubilant-engine/toggleScrolledFromTopClassToBody';
toggleScrolledFromTopClassToBody({});
toggleScrolledFromTopClassToBody({onLoad = true});
*/


import _ from 'lodash';

var body = document.querySelector('body');
var wasFirstOnloadScroll = false;

function process(breakpoint){
    if (window.pageYOffset  > breakpoint) {
        body.classList.add('scrolled-from-top');
    } else {
        body.classList.remove('scrolled-from-top');
    }
}

export default function toggleScrolledFromTopClassToBody({onLoad = false, breakpoint = 30}){
    if (window.pageYOffset <= breakpoint) {
        wasFirstOnloadScroll = true;
    }

    document.addEventListener('scroll', _.debounce(function(){
        
        if (!onLoad && !wasFirstOnloadScroll) {} else {
            process(breakpoint);}

        if (!wasFirstOnloadScroll) {
            wasFirstOnloadScroll = true;
        }
    }, 50));
}
