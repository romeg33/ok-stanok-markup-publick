export default function checkFileSize(inputFile, limitKB) {
    var correct = true;
    var files = inputFile.files;

    if (files.length > 0) {
        if (files[0].size > limitKB * 1024) {
            correct = false;
        }
    }
    return correct;
}
