import { TweenLite, TweenMax, TimelineLite} from "gsap";
import noUiSlider from 'nouislider';

// let d = c;

var tl = new TimelineLite();
var container = document.querySelector('.catalog-items-list');

var processInputChange = function processInputChange() {
    var chSelect = new Choices('.js-select-on-equipment', {
        search: false,
        sortFilter: function sortFilter() {
            return false;
        },
        callbackOnChange: function callbackOnChange(value, elSelect) {
            var el = document.querySelectorAll('.catalog-items-list__item');
            let sortData = value.split("-");
            let array = Array.from(el);

            array.sort(function(a, b) {
                let res = 1;
                if (
                    parseInt(a.attributes['data-' + sortData[0]].value) > parseInt(b.attributes['data-' + sortData[0]].value)
                ) {
                    res = 1;
                } else {
                    res = -1;
                }

                if (sortData[1] != 'asc') {
                    res = res * -1;
                }

                return res;
            });

            var angle = "2";
            var time = .15;
            var timeDelay = .05;
            let animation = tl.staggerTo(array, time, {
                opacity: "0",
                rotation: -angle
            }, timeDelay).to(array, 0, { rotation: angle })
            array.forEach(function(item) {
                item.remove();
                container.appendChild(item);
            });
            animation.staggerTo(Array.from(array).reverse(), time, {
                opacity: "1",
                rotation: "0"
            }, timeDelay);
        }
    });
};

let initFilterSelects = () => {
    //это дожно инициализироваться в форме
    // const chSelect = new Choices('.js-select-on-equipment-filter', {
    //         search: false,
    //         sortFilter: () => false,
    //     callbackOnChange: (value, elSelect) => false
    // });
}

let initFilterRange = () => {

    // в форме для этого будет колбэк
    // let sliderElement = document.getElementById('filter_price_range');
    // let maxVal = parseInt(sliderElement.getAttribute('data-max_val'));
    // let parentBlock = sliderElement.parentNode;
    // let minCounter = parentBlock.querySelector(
    //     '.input-block__range-min > .input-block__counter'
    // );
    // let maxCounter = parentBlock.querySelector(
    //     '.input-block__range-max > .input-block__counter'
    // );
    // let input = parentBlock.querySelector(
    //     '.input-block__range-input'
    // );
    //
    // let slider = noUiSlider.create(sliderElement, {
    //     start: [0, maxVal],
    //     connect: true,
    //     range: {
    //         'min': 0,
    //         'max': maxVal
    //     }
    // });
    //
    // slider.on('slide', function(values, handle, unencoded, isTap, positions) {
    //     let expr = /(\d)(?=(\d\d\d)+([^\d]|$))/g;
    //     let currMin = String(parseInt(unencoded[0]));
    //     let currMax = String(parseInt(unencoded[1]));
    //     minCounter.innerHTML = currMin.replace(expr, '$1 ');
    //     maxCounter.innerHTML = currMax.replace(expr, '$1 ');
    //     input.value = currMin + ':' + currMax;
    // });
}

export default function Equipment(){
    processInputChange();
    initFilterSelects();
    initFilterRange();
}
