// import { TweenLite, TweenMax, TimelineMax, Elastic } from "gsap";
import { TweenLite, TweenMax, TimelineLite} from "gsap";
let tl = new TimelineLite();
// import { TweenLite, TweenMax} from "gsap";

var cardSelector = '.card-manager';
var cardsContainer = document.querySelector('.jeContainer .cards');
var el = [];

Array.from(document.querySelectorAll(cardSelector)).forEach(function(item, i, arr) {
    el.push(item.cloneNode(true));
});
// TweenLite.to(el, 1, {x: "200px"});
// TweenLite.to(el, 1, {opacity: "0.5"});
// TweenMax.to(el, 1, {opacity: "0.5"});

let processInputChange = () => {
    const chSelect = new Choices('.js-select-on-managers', {
        search: false,
        sortFilter: () => false,
        callbackOnChange: (value, elSelect) => {
            value = value.trim();
            var currentItems = document.querySelectorAll(cardSelector);
            var selectedItems = [];

            for (var i = 0; i < el.length; i++) {
                var itemValue = el[i].attributes['data-spec-id'].value.trim();
                if (value == 0 || itemValue == value) {
                    selectedItems.push(el[i].cloneNode(true));
                }
            }
            
            let angle = "2";
            let time = .15;
            let timeDelay = .05;
            let tlObj = tl.staggerTo(currentItems, time, {
                opacity: "0",
                rotation: -angle
            }, timeDelay)
                .to(currentItems, 0, {rotation: angle});

            selectedItems.forEach(function(item, i, arr) {
                cardsContainer.appendChild(item);
            });

            tlObj.staggerTo(Array.from(selectedItems).reverse(), time, {
                opacity: "1",
                rotation: "0"
            }, timeDelay);

            currentItems.forEach(function(item, i, arr) {
                item.remove();
            });
        }
    });
}
export default function Managers(){
    processInputChange();
    
}