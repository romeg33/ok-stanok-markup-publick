import './Shared/Shared.js';
import './spares.scss';


import TieToggler from './Includes/tie-toggler/tie-toggler.js';
import TieHolder from './Includes/tie-holder/tie-holder';
import formFilterSpares from './Includes/form-filter-spares/form-filter-spares';
import tableSpares from './Includes/table-spares/table-spares';
// import stateStorageSpares from './Includes/state-storage-spares';    это уже вроде не нужно


let tieToggler = new TieToggler();
let tieHolder = new TieHolder();
tieToggler.on('afterhide', ()=>tieHolder.show());
tieHolder.on('afterhide', ()=> tieToggler.show());

formFilterSpares.init();
tableSpares.init();
// stateStorageSpares.init({form: formFilterSpares, table: tableSpares, tieToggler: tieToggler});   это уже вроде не нужно

formFilterSpares.getVals((vals)=> {
    tableSpares.updateValues(vals.items);
});

formFilterSpares.on('change', ()=> {
    formFilterSpares.getVals((vals)=> {

        document.querySelector('.spares-content-holder > .page-header').innerHTML = vals.title;
        document.querySelector('title').innerText = vals.title;

        tableSpares.updateValues(vals.items);
        // stateStorageSpares.save();   это уже вроде не нужно
    });
});
formFilterSpares.on('submit', ()=>{
    document.querySelector('.spares-content-holder').dispatchEvent(new Event('scroll-to-header', {bubbles: true}))
});

// just for testing
setTimeout(()=> {
    // tieToggler.hide();
}, 50);
document.querySelector('body').addEventListener('dblclick', () => {
    // tieToggler.show();
});
// just for testing end
