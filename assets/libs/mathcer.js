import MatchItem from './match-item';
import MatchWindow from './match-window';
import _ from 'lodash';

//Этот класс тоже использует хранилище в режиме rw
//Костыль из-за нехорошего UI-проектирования.
class Matcher {
    constructor({itemSelector}) {
        this._nCompareButton = document.querySelector('.global-compare-button');
        this._scope = this._nCompareButton.dataset.section;
        this._ajaxUrl = this._nCompareButton.dataset.href;
        this._itemSelector = itemSelector;
        this._items = [];
        this._state = 'passive';    //active/passive        если passive, то глобальная кнопка сравнения не работает.
        this._qtyActiveItems = 0;   //обращатся через геттер/сеттер:
        this._window = new MatchWindow({});
        this._window.on('afterClose', this._deactivateAllItems.bind(this));

        this._buttonListener = this._buttonListener.bind(this);
        this._initItems();
    };

    get qtyActiveItems() {
        return this._qtyActiveItems;
    }

    set qtyActiveItems(val) {
        this._qtyActiveItems = val;
        this._updateState();
    }

    _getQtyActiveItems() {
        return this._getItemIdsArr().length;
    }

    _getItemIdsArr() {
        let arr;
        let val = window.localStorage.getItem('matcher-' + this._scope);
        (val == null) ? arr = [] : arr = JSON.parse(val);
        return arr;
    }

    _resetItemIdsArr() {
        this._setItemIdsArr([]);
    }
    _setItemIdsArr(arr) {
        localStorage.setItem('matcher-' + this._scope, JSON.stringify(arr));
    }
    
    reInit() {
        this._items = [];       //теоретически сборщик мусора должен сработать.
        this._initItems();
    }


    getScope() {
        return this._scope;
    }

    _initItems() {
        let nItems = document.querySelectorAll(this._itemSelector);

        Array.from(nItems).forEach((nItem)=> {
            let newMatchItem = new MatchItem({node: nItem, matcher: this});
            this._items.push(newMatchItem);

            newMatchItem.on('activate', ()=> {
                this.qtyActiveItems += 1;
            })
            newMatchItem.on('deactivate', ()=> {
                this.qtyActiveItems -= 1;
            })
        })
        this.qtyActiveItems = this._getQtyActiveItems();
    }

    _updateState() {
        if (this._qtyActiveItems > 1 && this._state == 'passive') {
            this._activate();
        }
        if (this._qtyActiveItems < 2 && this._state == 'active') {
            this._deactivate();
        }
    }

    _activate() {
        this._nCompareButton.classList.remove('disabled');
        this._nCompareButton.addEventListener('click', this._buttonListener);
        this._state = 'active';
    }

    _deactivate() {
        this._nCompareButton.classList.add('disabled');
        this._nCompareButton.removeEventListener('click', this._buttonListener);
        this._state = 'passive';
    }

    _buttonListener() {
        fetch(this._ajaxUrl, {
            method: 'POST',
            body: JSON.stringify(this._getItemIdsArr()),
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        })
        .then((res)=> {
            return res.json();
        })
        .then((data) => {
            (data.status == 0) ? this._processResponseError(data) : this._processResponseSuccess(data);
        })
    }

    _processResponseError(data) {
        this._deactivateAllItems();
        this._window.show(data);
    }

    _deactivateAllItems(){
        //Деактивировать те, которые есть на странице (потжимать кнопки)
        this._items.forEach((item)=> {
            item.deactivate();
        });
        //удалить оставшиеся из хранилища
        this._resetItemIdsArr();
        //сбросить счетчик (сеттер вызовет обновлнение глобальной кнопки "сравнить")
        this.qtyActiveItems = 0;
    }
    _processResponseSuccess(data) {
        //Здесь предполагается что с бэкэнда не могут прити элементы, которые не были отпраленны на бэкэнд
        let idsFromResponse = data.items.map((item)=> {
            return item.id;
        });
        let idsPresent = this._getItemIdsArr();
        let idsToLeave = idsPresent.filter((item, index, arr)=> {
            let res = false;
            if (idsFromResponse.indexOf(item) > -1) {
                res = true;
            }
            return res;
        })
        //сделать массив общих значений из idsToLeave и idsFromResponse
        idsToLeave = _.uniq(idsToLeave.concat(idsFromResponse));

        //1.отключение итемов, которых нет в idsToLeave
        this._items.forEach((item)=> {
            idsToLeave.indexOf(item.getItemId())
            if (idsToLeave.indexOf(item.getItemId()) == -1) {
                item.deactivate();
            }
        });
        
        //2.запихнуть idsToLeave в хранилище
        this._setItemIdsArr(idsToLeave);
        
        //3.поставить счетчик итемов исходя из кол-ва в хранилище
        this.qtyActiveItems = this._getQtyActiveItems();
        
        //4.включить итемы из хранилища. (на случай если с бекенда пришло то, что не было отмечено)
        this._items.forEach((item)=>{
           if (
               !item.isActive()
               && idsToLeave.indexOf(item.getItemId()) > -1
           ){
               item.activate();
           } 
        });

        //показать окно
        this._window.show(data);
    }

}

export default Matcher