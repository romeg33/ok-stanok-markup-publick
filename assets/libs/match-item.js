import MatchItemDB from './match-item-localstorage';


//delme
class MatchItem {
    constructor({node, matcher}){
        this._node = node;
        this._matcher = matcher;
        this._db = new MatchItemDB(this);
        this._compareButton = this._node.querySelector('.compare-button');
        this._state = 'passive';   //active/passive
        this._listeners = {
            'activate': [],
            'deactivate': []
        }
        this._setStateWithDB();
        // console.log(this._state);
        this._initUserActions();
    }
    
    _setStateWithDB(){
        if (this._db.check()) {
            this._state = 'active';
            this._compareButton.classList.add('compare-button--active');
        }
    }
    _initUserActions(){
        this._compareButton.addEventListener('click', (e)=>{
            (this._state == 'passive') ? this.activate() : this.deactivate();
        })
    }
    
    isActive(){return this._state == 'active' ? true : false}

    getItemId(){return this._node.dataset.itemId;}
    getScope(){return this._matcher.getScope();}
    activate(){
        if (this._state == 'active') return;
        this._db.add();
        this._state = 'active';
        this._compareButton.classList.add('compare-button--active');
        this._runListeners('activate');
    }
    deactivate(){
        if (this._state == 'passive') return;
        this._db.remove();
        this._state = 'passive';
        this._compareButton.classList.remove('compare-button--active');
        this._runListeners('deactivate');
    }
    _runListeners(eName){
        this._listeners[eName].forEach((cb)=>{
            cb();
        })
    }
    on(eventName, cb){
        this._listeners[eventName].push(cb);
    }
}
export default MatchItem;