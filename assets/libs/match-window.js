import makeNode from '../jubilant-engine/createNodeFromHTMLString';
import picoModal from 'picomodal';
import Ps from 'perfect-scrollbar';


class MatchWindow {
    constructor({}) {
        this._data = {} //устанавливается методом show
    }

    //используемые события:
    //afterClose
    //observer pattern
    on(eventName, cb){
        if (!this._listeners) this._listeners = [];
        if (!this._listeners[eventName]) this._listeners[eventName] = [];
        this._listeners[eventName].push(cb);
    }
    _runListeners(eName){this._listeners[eName].forEach((cb)=>{cb();})}
    //observer pattern end
    
    
    show(data) {
        this._data = data;
        let nodeToInsert = (this._data.status == 0)
            ? this._createNodeError()
            : this._createNodeMatch();

        let modal = picoModal({
            content: nodeToInsert,
            // bodyOverflow: true, //чтобы бади не прокручивалось; def: true
            bodyOverflow: false, //чтобы бади не прокручивалось; def: true
            overlayStyles: {
                backgroundColor: 'black',
                opacity: '0',
                transition: 'opacity 0.5s'
            },
            modalStyles: {
                backgroundColor: 'transparent',
                // outline: '1px solid green',
                // "overflow-y": 'auto',    //если без perfect scrollbar
                "overflow": 'hidden',       //если с perfect scrollbar
                opacity: '0',
                transition: 'opacity 0.5s'
        
            }
        });
        let body = document.querySelector('body');
        
        modal.afterClose(()=>{
            modal.destroy();
            this._runListeners('afterClose');
        })
        .afterShow((modal)=>{
            modal.modalElem().style.opacity = 1;
            modal.overlayElem().style.opacity = 0.5;
            Ps.initialize(modal.modalElem());
            modal.modalElem().addEventListener('wheel', function (e) {
                e.preventDefault();
                e.stopPropagation();
                let steps = 7, ms = 100, distanceMultiplier = 1;
                for (var i = 0; i < steps; i++) {
                    setTimeout(() => {
                        let delta = (e.deltaY > 0) ? 100 : -100;
                        modal.modalElem().scrollTop += (delta / steps * distanceMultiplier);  
                    }, ms * i * (1 / steps))
                }
            }, true);
        })
        .beforeClose((modal, e)=>{
            e.preventDefault();
            modal.modalElem().style.opacity = 0;
            modal.overlayElem().style.opacity = 0;
            setTimeout(()=>{modal.forceClose()}, 700)
            
        });
        modal.show();
    }

    _createNodeError() {
        let res = this._data.content;


        res = this._wrapToOneNode(res);
        res.classList.add('status-0');
        return res;
    }

    _createNodeMatch() {
        let res = '';

        res += this._createHeader();
        res += this._createRows();

        res = this._wrapToOneNode(res);
        
        this._attachListeners(res);
        return res;
    }

    _attachListeners(oneNode) {
        let crosses = oneNode.querySelectorAll('.cross');
        Array.from(crosses).forEach((cross)=> {
            cross.addEventListener('click', this._processCrossClick.bind(this, oneNode))
        });
    }

    _processCrossClick(oneNode, e) {
        let itemId = e.target.parentNode.parentNode.dataset.itemId;
        // console.log(itemId);
        // console.log(oneNode);
        let nlToDelete = oneNode.querySelectorAll(`[data-item-id="${itemId}"]`);
        Array.from(nlToDelete).forEach((node)=> {
            node.parentNode.removeChild(node);
        })
        console.log(nlToDelete);
    }

    _wrapToOneNode(html) {
        return makeNode(`<div class="match-content">${html}</div>`)
    }

    _createHeader() {
        let name = '<div class="name"><p>Сравнить Оборудование</p></div>';

        let items = this._createHeaderItems();
        let res = `<div class="head">${name}${items}</div>`;

        return res;
    }

    _createHeaderItems() {
        let items = this._data.items;
        let res = '';
        items.sort((a, b)=> {
            if (a.priority < b.priority) return 1;
            if (a.priority > b.priority) return -1;
            return 0;
        });
        items.forEach((item)=> {
            res += `<div class="val" data-item-id="${item.id}">
                        <div class="box"> <div class="cross" style="display: none"></div>
                            ${item.title}
                        </div>
                    </div>`;
        })
        return res;
    }

    _createRows() {
        let res = '';
        let features = this._data.features;
        features.sort((a, b)=> {
            if (a.priority < b.priority) return 1;
            if (a.priority > b.priority) return -1;
            return 0;
        });
        features.forEach((feature)=> {
            res += this._createRow(feature);
        });
        return res;
    }

    _createRow(feature) {
        let name = `<div class="name"><p>${feature.title}</p></div>`
        let redClass = (feature['is-red'] == 1 ? 'red' : '');

        let rowVals = this._createRowVals(feature);
        let res = `<div class="row ${redClass}">${name}${rowVals}</div>`;
        return res;
    }

    _createRowVals(feature) {
        let res = '';
        let items = this._data.items;
        items.sort((a, b)=> {
            if (a.priority < b.priority) return 1;
            if (a.priority > b.priority) return -1;
            return 0;
        });
        items.forEach((item)=> {
            let currentFeatureVal = item.features.filter((itemsFeature, index, arr)=> {
                if (itemsFeature.id == feature.id) return true;
                return false;
            });
            if (!currentFeatureVal.length) {
                currentFeatureVal = '-';
            } else {
                currentFeatureVal = currentFeatureVal[0].val
            }

            res += `<div class="val" data-item-id="${item.id}">
                        <p>
                            ${currentFeatureVal}
                        </p>
                    </div>`;
        })
        return res;
    }
}
export default MatchWindow;