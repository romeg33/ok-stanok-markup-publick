import './Shared/Shared.js';
import './equipment2.scss';
window.jQuery = require('jquery');
import Equipment from  './Layout/Equipment';
Equipment();

import TieToggler from './Includes/tie-toggler/tie-toggler.js';
import TieHolder from './Includes/tie-holder/tie-holder';
import formFilterEquipment from './Includes/form-filter-equipment/form-filter-equipment';
import Matcher from './libs/mathcer';

let tieToggler = new TieToggler();
let tieHolder = new TieHolder();
let matcher = new Matcher({itemSelector: '.catalog-items-list__item'});

tieToggler.on('afterhide', ()=>tieHolder.show());
tieHolder.on('afterhide', ()=> tieToggler.show());

formFilterEquipment.init();

// formFilterEquipment.getVals((vals)=> {
//     //сюда колбэк для записи данных в таблицу с оборудованием
//     //vals - значения, полученные аяксом с сервера
// });

function updateCatalogData(vals) {
    document.querySelector('.spares-content-holder > .page-header').innerHTML = vals.title;
    document.querySelector('title').innerText = vals.title;
    document.querySelector('.catalog-description__text').innerHTML = vals.text_full;
    
    //vals - значения, полученные аяксом с сервера
    let container = document.querySelector('.catalog-items-list');
    container.innerHTML = '';
    let div = document.createElement('div');
    if (vals.items.length) {
        Array.from(vals.items).forEach(function (item) {
            div.innerHTML = div.innerHTML + item;
        });
        container.innerHTML = div.innerHTML;
    }

    
    if (vals.inputs && vals.inputs['tipy'] != undefined) {
        let ins = window.jQuery(vals.inputs['tipy']);
        let insClass = ins.attr('class');
        let isset = document.querySelector('.' + insClass);
        if (isset) {
            isset.outerHTML = vals.inputs['tipy'];
        } else {
            window.jQuery('.form-filter-spares__equipment-type').after(ins);
        }
        
        let inputs = document.querySelectorAll('.' + insClass + ' input');
        Array.from(inputs).forEach((input) => {
            formFilterEquipment._initChangeCbsFor(input);
        })
    }
}

formFilterEquipment.on('change', ()=> {
    // console.log('changed');
    formFilterEquipment.getVals((vals)=> {
        //сюда колбэк для обновления данных в таблице с оборудованием
        
        updateCatalogData(vals);
        matcher.reInit();

    });
});
formFilterEquipment.on('submit', ()=> {
    document.querySelector('.spares-content-holder').dispatchEvent(new Event('scroll-to-header', {bubbles: true}))
});


// just for testing
// matcher._buttonListener();
// setTimeout(()=> {
//     tieToggler.hide();
// }, 50);
// document.querySelector('body').addEventListener('dblclick', () => {
//     tieToggler.show();
// });
// just for testing end

