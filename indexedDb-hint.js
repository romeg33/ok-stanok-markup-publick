
function connectDB(cb = ()=>{}){
    var req = indexedDB.open('dbName', 1);  //IDBOpenDBRequest, extends IDBRequest
    req.onerror = (e)=>{//e.type == "error"};        //extends IDBRequest
    req.onsuccess = function () {                    //extends IDBRequest
        let db = req.result;
        db.onerror = function(e) {e.target.errorCode};
        cb(db);
        db.close();
    }
    req.onblocked = function() {console.log("can't onupgradeneeded: db is open.");}
    req.onupgradeneeded = function(IDBVersionChangeEvent){
        IDBVersionChangeEvent.newVersion    //ro, number
        IDBVersionChangeEvent.oldVersion    //ro, number
        let db = IDBVersionChangeEvent.target.result;   //IDBDatabase, implements EventTarget
        
        db.deleteObjectStore('store-name');
        db.createObjectStore("store-name", { keyPath: "keyPath", autoIncrement: true}); //ret: IDBObjectStore
        db.transaction(['store-name', 'store-name2'], "readwrite");    //ret: IDBTransaction extends EventTarget
        db.objectStoreNames // DOMSttingList ro
        db.name //ro
        db.version //ro
        db.onabort = ()=>{}
        db.onclose = (e)=>{}
        db.onerror = (e)=>{}
        db.onversionchange = (e)=>{}
        // storeMatcherItems.createIndex("indexName", "indexFieldName", { unique: false });
        connectDB(cb);
    }
}
connectDB((db)=>{ 
    let trans = db.transaction(['store-name'], "readwrite");
    let store = trans.objectStore('store-name');
});




//indexedDB.deleteDatabase('DB NAME') - удалить базу из консоли

let dbName = 'world-equipment';
let myStoreName = 'myStoreName';

connectDB(function(db){
    //ro - default
    var req = db.transaction([storeName], "readonly").objectStore(storeName).get(file);
    req.onsuccess = function(){req.result;}
    var req = db.transaction([storeName], "readwrite").objectStore(storeName).put(file);
    req.onsuccess = function(){return req.result;}
    db.transaction([storeName], "readwrite").objectStore(storeName).delete(file);


    for (var i in customerData) {
        store.add(customerData[i]);
    }
});

function getAllStore(f){
    connectDB(function(db){
        var rows = [];
        var store = db.transaction([storeName], "readonly").objectStore(storeName);
        
        store.openCursor().onsuccess = function(e) {
            var cursor = e.target.result;
            if(cursor){
                rows.push(cursor.value);
                cursor.continue();
            }
            else {
                f(rows);
            }
        };
    });
}