<?
require_once '../../vendor/autoload.php';
$lipsum = new joshtronic\LoremIpsum();
$res = [];
$qty = rand(100, 500);
//$qty = 2;
$qty = 300;

for ($i = 1; $i <= $qty; $i++){
    $item = [
        "articul" => rand(1, 100000),
        "link" => "/dummy-link",
        "name" => $lipsum->words(rand(1, 15)),
//        "description" => $lipsum->words(rand(1, 35)),
        "description" => $lipsum->words(rand(1, 85)),
//        "description" => $lipsum->words(rand(1, 15)),
        "price" => rand(100, 10000000) / 100,
        "img" => "https://dummyimage.com/600x400/" . rand(0, 999) . "/fff&text=". $lipsum->word() . "",

    ];
    array_push($res, $item);
}

echo json_encode([
    'items' => $res,
    'title' => 'Заголовок ' . time(),
    'text_full' => 'текст' . time()
]);
