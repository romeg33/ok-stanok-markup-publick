<?
//это просто скопировал из запчастей, чтобы какие-нибудь данные возвращались для примера aaaaaaaaaa
require_once '../../vendor/autoload.php';
$lipsum = new joshtronic\LoremIpsum();
$res = [];


if ($_POST['equipment-type'] == 'empty') {
    $i = 1;
    $qty = $i + 20;
} elseif ($_POST['equipment-type'] == 'Чебурашка') {
    $i = 1;
    $qty = $i + 10;
} elseif ($_POST['equipment-type'] == 'Крокодил Гена') {
    $i = 11;
    $qty = $i + 10;
}

for (; $i <= $qty; $i++){
    $item = <<<HTML
<div class="catalog-items-list__item" data-item-id="{$i}" data-price="882000" data-priority="100" data-size="">

    <div class="catalog-list-item">

        <a href="http://test.ok-stanok.ru/oborudovanie/shredery/192-shreder-sjz-600" class="catalog-list-item__big-link">

            <div class="catalog-list-item__title">
                Элемент {$i}            </div>

            <div class="catalog-list-item__content">

                <img src="http://test.ok-stanok.ru/data/temp/936dfa0fcdca05c1ccba10c4fe0a4180.jpg" alt="" class="catalog-list-item__image">
                
                <div class="catalog-list-item__price">
                    <p>₽ 1 279 359</p>
                </div>
            </div>

        </a>

        <div class="catalog-list-item__footer">

            <a href="/oborudovanie/shredery/192-shreder-sjz-600" class="catalog-list-item__more-link">Подробней</a>

            <div class="catalog-list-item__compare-button">

                <div class="compare-button">

                    <div class="compare-button__description">
                        <div class="compare-button__v-line"></div>
                        Сравнить
                    </div>

                    <div class="compare-button__icon"></div>

                </div>

            </div>
        </div>

    </div>

</div>
HTML;

    array_push($res, $item);
}

echo json_encode([
    'items' => $res,
    'title' => 'Заголовок ' . time(),
    'text_full' => 'текст' . time()
]);
