<?

$_POST = json_decode(file_get_contents("php://input"), true);

$isCorrect = in_array(1 , $_POST) ? false : true;

require_once '../../vendor/autoload.php';
$lipsum = new joshtronic\LoremIpsum();
$res = [];

if (!$isCorrect){
    $html = <<<HTML
<div class="compare-result__errorText">
    <div class="compare-result__message">
  		<div class="compare-result__title">Неверные данные для сравнения</div>
        <div class="compare-result__just-text">* какаято хрень тут написана, чуть больше чем выше</div>
	</div>
</div>
HTML;
    $res = [ 
        "status" => "0",
        "content" => $html,
        "post" => $_POST,
    ];
} else {
    $res = [
        "status" => "1",
        "items" => [
            [   "priority" => "3",     "id" => "2", "title" => "Стренгорез ножевой, PG1.5H",
                "features" => [
                    ["id" => "101", "val" => "Белый"],
                    ["id" => "102", "val" => "Квадратный"],
                    ["id" => "103", "val" => "Высокий"],
                    ["id" => "104", "val" => "fsd sdofyd sdfau d sdfuoi wearw"],

                    ["id" => "201", "val" => "Белый"],
                    ["id" => "202", "val" => "Квадратный"],
                    ["id" => "203", "val" => "Высокий"],
                    ["id" => "204", "val" => "fsd sdofyd sdfau d sdfuoi wearw"],
                ],
            ],
            [   "priority" => "1",     "id" => "3", "title" => "Товар №1",
                "features" => [
                    ["id" => "101", "val" => "Черный"],
                    ["id" => "102", "val" => "Круглый"],
                    ["id" => "103", "val" => "Низкий"],
                    ["id" => "104", "val" => "Хороший"],
                    
                    ["id" => "201", "val" => "Белый"],
                    ["id" => "202", "val" => "Квадратный"],
                    ["id" => "203", "val" => "Высокий"],
                    ["id" => "204", "val" => "fsd sdofyd sdfau d sdfuoi wearw"],
                ],
            ],
            [   "priority" => "2",     "id" => "12", "title" => "Второй",
                "features" => [
                    ["id" => "101", "val" => "Черный"],
                    ["id" => "102", "val" => "Круглый"],
                    ["id" => "103", "val" => "Низкий gfs fgsdj fdsg dfsg fdsg fsd fds "],
                    ["id" => "104", "val" => "Плохой"],

                    ["id" => "201", "val" => "Белый"],
                    ["id" => "202", "val" => "Квадратный"],
                    ["id" => "203", "val" => "Высокий"],
                    ["id" => "204", "val" => "fsd sdofyd sdfau d sdfuoi wearw"],
                ],
            ],
        ],
        "features" => [
            [
                "priority" => "2",
                "id" => "101",
                "title" => "Цвет",
                "is-red" => "1",
            ],
            [
                "priority" => "4",
                "id" => "102",
                "title" => "Форма",
                "is-red" => "0",
            ],
            [
                "priority" => "1",
                "id" => "103",
                "title" => "Высота",
                "is-red" => "0",
            ],
            [
                "priority" => "3",
                "id" => "104",
                "title" => "Качество fad fsg fdg lkfg lfg fds",
                "is-red" => "1",
            ],
            
            [
                "priority" => "2",
                "id" => "201",
                "title" => "Цвет",
                "is-red" => "1",
            ],
            [
                "priority" => "4",
                "id" => "202",
                "title" => "Форма",
                "is-red" => "0",
            ],
            [
                "priority" => "1",
                "id" => "203",
                "title" => "Высота",
                "is-red" => "0",
            ],
            [
                "priority" => "3",
                "id" => "204",
                "title" => "Качество fad fsg fdg lkfg lfg fds",
                "is-red" => "1",
            ],
        ],
        "post" => $_POST,
    ];
    
}

//$qty = 60;
//
//for ($i = 1; $i <= $qty; $i++){
//    $item = [
//        "foo" => 'bar',
//    ];
//    array_push($res, $item);
//}
echo json_encode($res);